var pollData;

jQuery.ajax({
  type: "POST",
  url: 'showPoll.php',
  dataType: 'json',
  data: {
    functionname: 'showPoll',
    PollID: pollID
  },
  success: function (obj, textstatus) {
            pollData = obj;
            loadPoll();
          }
});

function replaceSVG(){
  jQuery.get('images/checkmark.svg', "", function(data) {
      $(".icon-replace-checkmark").replaceWith(data);
  }, "html");

  jQuery.get('images/checkmark-tentative.svg', "", function(data) {
      $(".icon-replace-checkmark-tentative").replaceWith(data);
  }, "html");

  jQuery.get('images/location.svg', "", function(data) {
      $(".icon-replace-location").replaceWith(data);
  }, "html");

  jQuery.get('images/description.svg', "", function(data) {
      $(".icon-replace-description").replaceWith(data);
  }, "html");

  jQuery.get('images/edit-pen.svg', "", function(data) {
      $(".icon-replace-edit-pen").replaceWith(data);
  }, "html");
}

function checkOverflow(el)
{
   var curOverflow = el.style.overflow;
   if ( !curOverflow || curOverflow === "visible" )
      el.style.overflow = "hidden";
   var isOverflowing = el.clientWidth < el.scrollWidth
      || el.clientHeight < el.scrollHeight;
   el.style.overflow = curOverflow;
   return isOverflowing;
}

function resize(){
  console.log("resizing");
  if(checkOverflow(document.getElementById('poll'))){
    $("#poll").removeClass("no-scrollbar");
  }else{
    $("#poll").addClass("no-scrollbar");
  }
  if(editParticipant) $("#abort-edit-wrapper").css("width", Math.min($("#poll table").width(),$("#poll").width())+"px");
}

window.onresize = resize;
//window.onload = resize;

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}

////////////////////

function changeCheckboxState(checkboxID){
  nextClass = "";
  if($("#date-checkbox-" + checkboxID).hasClass("going")) nextClass = "going-tentative";
  if($("#date-checkbox-" + checkboxID).hasClass("going-tentative")) nextClass = "not-going";
  if($("#date-checkbox-" + checkboxID).hasClass("not-going")) nextClass = "going";
  $("#date-checkbox-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
  $("#date-checkbox-" + checkboxID).addClass(nextClass);

  $("#display-date-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
  $("#display-date-" + checkboxID).addClass(nextClass);

  $("#date-availability-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
  $("#date-availability-" + checkboxID).addClass(nextClass);
}

////////////////////


var allDates = [];
var availabilityAllDates = [];
var editParticipant = false;
var editParticipantID = -1;

function createPollRow(){
  // top row with dates
  htmlOutput = "<tr id='dates-row'>";
  htmlOutput += "<td></td>";
  allDates.forEach((date, i) => {
    displayDate = date.toDateString().split(' ');
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    htmlOutput += "<td>";
      htmlOutput += "<div class='display-date' id='display-date-" + dateColumn + "'>";
        htmlOutput += "<div class='display-date-month'>" + displayDate[1] + "</div>";
        htmlOutput += "<div class='display-date-day'>" + displayDate[2] + "</div>";
        htmlOutput += "<div class='display-date-weekday'>" + displayDate[0] + "</div>";
      htmlOutput += "</div>";
    htmlOutput +=  "</td>";
  });
  htmlOutput += "</tr>";

  // row that shows how many people are available
  htmlOutput += "<tr id='availability-row'>";
  htmlOutput += "<td></td>";
  allDates.forEach((date, i) => {
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    htmlOutput += "<td><div class='display-date' id='date-availability-" + dateColumn + "'>0</div></td>";
  });
  htmlOutput += "</tr>";

  // row for checkboxes and name
  htmlOutput += "<tr id='checkbox-row'>";
  htmlOutput += "<td><div><input type='text' id='participant-name'></input><br>";
  if(!editParticipant) htmlOutput += "<button id='submit' onclick='submitDates()'>Send</button>";
  else htmlOutput += "<button id='submit' onclick='replaceDates("+editParticipantID+")'>Update</button>";
  htmlOutput += "</div></td>";
  allDates.forEach((date, i) => {
    displayDate = date.toDateString();
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    //htmlOutput += displayDate + ", " + stringDate + "<br>";
    //dateCheckbox = "<td><input type='checkbox' id='date-checkbox-" + dateColumn +"'>"+/*displayDate+*/"</input></td>"

    dateCheckbox = $("#copy-triple-state-checkbox").clone();
    dateCheckbox.removeAttr('id');
    dateCheckbox.attr('id', "date-checkbox-" + dateColumn);
    dateCheckbox.attr('onclick', 'changeCheckboxState("' + dateColumn + '")');
    dateCheckbox = dateCheckbox[0].outerHTML;

    htmlOutput += "<td>" + dateCheckbox + "</td>";
  });
  return htmlOutput + "</tr>";
}

function createParticipantRow(participant){

  if(editParticipant && ""+participant.PersonID == ""+editParticipantID) return "";

  htmlOutput = "<tr class='participant'>"
  htmlOutput += "<td>"+participant.PersonName;
  htmlOutput += "<button class='edit-button' onClick='reloadPollEdit("+ participant.PersonID + ")'>";
  htmlOutput += "<div class='icon-replace-edit-pen'></div>";
  htmlOutput += "</button>";
  htmlOutput += "</td>";
  allDates.forEach((date, i) => {
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    value = participant["date_"+dateColumn];

    greenCheckboxElement = $("#copy-green-checkmark").clone();
    greenCheckboxElement.removeAttr('id');
    greenCheckboxElement = greenCheckboxElement[0].outerHTML;

    orangeCheckboxTentativeElement = $("#copy-orange-checkmark-tentative").clone();
    orangeCheckboxTentativeElement.removeAttr('id');
    orangeCheckboxTentativeElement = orangeCheckboxTentativeElement[0].outerHTML;

    if(value == "1") {
      availabilityAllDates[i]++;
      htmlOutput += "<td>" + greenCheckboxElement + "</td>";
    }else if(value == "2"){
      availabilityAllDates[i]++;
      htmlOutput += "<td>" + orangeCheckboxTentativeElement + "</td>";
    }else{
      htmlOutput += "<td></td>";
    }
    //htmlOutput += "<td style='background-color:"+ (value == "1" ? "#00ff00" : "#ff0000") + "'></td>"
  });
  return htmlOutput + "</tr>";
}

function createPollTable(){
  var startDate = new Date(pollData.pollSettings.StartDate);
  var endDate = new Date(pollData.pollSettings.EndDate);
  allDates = getDates(startDate, endDate);
  availabilityAllDates = Array(allDates.length).fill(0);
  var pollHTML = "<table>";
  //create header
  pollHTML += createPollRow();
  //create already submitted Entries
  pollData.participantData.forEach((participant, i) => {
    pollHTML += createParticipantRow(participant);
  });
  pollHTML += "</table>"
  pollHTML += "<div id='poll-divider'></div>"
  //display Poll
  $("#poll").html(pollHTML);
  $("#poll-divider").css('width', $("#poll table").width()-10);

  //tally availability
  allDates.forEach((date, i) => {
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    $("#date-availability-" + dateColumn).html(availabilityAllDates[i]);
  });

  resize();
  replaceSVG();
}

function loadPoll(/*pollData*/){
  console.log(pollData);
  $("#poll-title").append(pollData.pollSettings.PollTitle);
  $("#poll-location").append(pollData.pollSettings.PollLocation);
  $("#poll-description").append(pollData.pollSettings.PollDescription);
  createPollTable();
}

function getCheckboxAvailability(checkboxID){
  if($("#date-checkbox-" + checkboxID).hasClass("going")) return 1;
  if($("#date-checkbox-" + checkboxID).hasClass("going-tentative")) return 2;
  if($("#date-checkbox-" + checkboxID).hasClass("not-going")) return 0;
  return 0;
}

function reloadPollEdit(participantID){
  editParticipantID = participantID;
  editParticipant = true;
  var editParticipantData;
  pollData.participantData.forEach((participant, i) => {
    if(""+participant.PersonID == ""+participantID) {
      editParticipantData = participant;
      return;
    }
  });
  $("#poll").html("");
  createPollTable();
  $("#participant-name").val(editParticipantData.PersonName);
  var startDate = new Date(pollData.pollSettings.StartDate);
  var endDate = new Date(pollData.pollSettings.EndDate);
  allDates = getDates(startDate, endDate);
  allDates.forEach((date, i) => {
    checkboxID = date.toISOString().split('T')[0].replaceAll("-","");
    var availability = editParticipantData["date_"+checkboxID];
    var availabilityClass = availability == "1" ? "going" : (availability == "2" ? "going-tentative" : "not-going");
    $("#date-checkbox-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
    $("#date-checkbox-" + checkboxID).addClass(availabilityClass);
    $("#display-date-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
    $("#display-date-" + checkboxID).addClass(availabilityClass);
    $("#date-availability-" + checkboxID).removeClass(["going","not-going","going-tentative"]);
    $("#date-availability-" + checkboxID).addClass(availabilityClass);
  });

  $("body").append("<div id='abort-edit-wrapper'><button id='abort-edit' onClick='cancelReplace()'>Cancel Editing</button></div>");
  $("#abort-edit-wrapper").css("width", Math.min($("#poll table").width(),$("#poll").width())+"px");
}

function submitDates(){
  var availabilityData = [];
  allDates.forEach((date, i) => {
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    dateAvailability = getCheckboxAvailability(dateColumn);
    availabilityData.push(dateAvailability);
  });

  if($('#participant-name').val() == ""){
    console.log("Name can't be left empty");
    return;
  }

  jQuery.ajax({
    type: "POST",
    url: 'showPoll.php',
    dataType: 'json',
    data: {
      functionname: 'submitDates',
      PollID: pollID,
      name: $("#participant-name").val(),
      data: availabilityData
    },

    success: function (obj, textstatus) {
              console.log(obj);
              location.reload();
            }
  });
}

function cancelReplace(){
  location.reload();
}

function replaceDates(participantID){
  var availabilityData = [];
  allDates.forEach((date, i) => {
    dateColumn = date.toISOString().split('T')[0].replaceAll("-","");
    dateAvailability = getCheckboxAvailability(dateColumn);
    availabilityData.push(dateAvailability);
  });

  if($('#participant-name').val() == ""){
    console.log("Name can't be left empty");
    return;
  }

  jQuery.ajax({
    type: "POST",
    url: 'showPoll.php',
    dataType: 'json',
    data: {
      functionname: 'replaceDates',
      PollID: pollID,
      name: $("#participant-name").val(),
      ParticipantID: participantID,
      data: availabilityData
    },

    success: function (obj, textstatus) {
              console.log(obj);
              location.reload();
            }
  });
}
