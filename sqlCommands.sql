CREATE TABLE poll1 (
    PersonID int NOT NULL AUTO_INCREMENT,
    PersonName varchar(255),
    availabilityDate1 int,
    availabilityDate2 int,
    availabilityDate3 int,
    availabilityDate4 int,
    PRIMARY KEY (PersonID)
);

CREATE TABLE polls (
    PollID varchar(255) NOT NULL,
    PollTitle text,
    PollLocation text,
    PollDescription text,
    StartDate date,
    EndDate date,
    PRIMARY KEY (PollID)
);




INSERT INTO poll1 VALUES(DEFAULT, "Stephan", 1, 1, 1, 1);
