<?php

include "serverConnection.php";
require "libraries/HTMLPurifier/HTMLPurifier.auto.php";

header('Content-Type: application/json; charset=utf-8');

function showPoll($pollID){

  $conn = OpenCon();
  $pollTable = "poll_".$pollID;

  $sql = "SELECT * FROM ".$pollTable;
  $participantData = array();
  $result = $conn->query($sql);
  while($row = $result->fetch_assoc()) {
      $participantData[] = $row;
  }

  $sql = "SELECT * FROM polls WHERE PollID='".$pollID."'";
  $pollSettings = array();
  $result = $conn->query($sql);
  while($row = $result->fetch_assoc()) {
      $pollSettings[] = $row;
  }

  $pollSettings = $pollSettings[0];

  $pollData = [
    'pollSettings' => $pollSettings,
    'participantData' => $participantData
  ];

  echo json_encode($pollData);

  CloseCon($conn);
}


function submitDates($PollID, $name, $data){
  $conn = OpenCon();

  $config = HTMLPurifier_Config::createDefault();
  $config->set('HTML.Allowed', 'i,b,strong,em,small,del,ins,sub,sup');
  $purifier = new HTMLPurifier($config);
  $cleanName =  $purifier->purify( strval($name) );
  $cleanName = str_replace("'", "''", $cleanName);
  if(strlen($cleanName) == 0) { CloseCon($conn); return;}

  $sql = "INSERT INTO poll_".$PollID." VALUES(DEFAULT, '".$cleanName."'";
  foreach($data as $value){
    if(!is_numeric($value)) { CloseCon($conn); return;}
    $sql = $sql.",".$value;
  }
  $sql = $sql.");";
  $conn->query($sql);
  //echo json_encode($sql);
  echo json_encode("successfully added ".$name."'s dates!");
  CloseCon($conn);
}

function replaceDates($PollID, $name, $ParticipantID, $data){
  $conn = OpenCon();

  $config = HTMLPurifier_Config::createDefault();
  $config->set('HTML.Allowed', 'i,b,strong,em,small,del,ins,sub,sup');
  $purifier = new HTMLPurifier($config);
  $cleanName =  $purifier->purify( strval($name) );
  $cleanName = str_replace("'", "''", $cleanName);
  if(strlen($cleanName) == 0) { CloseCon($conn); return;}

  $sql = "INSERT INTO poll_".$PollID." VALUES(".$ParticipantID.", '".$cleanName."'";
  foreach($data as $value){
    if(!is_numeric($value)) { CloseCon($conn); return;}
    $sql = $sql.",".$value;
  }
  $sql = $sql.");";

  $remsql = "DELETE FROM poll_".$PollID." WHERE PersonID = ".$ParticipantID;
  $conn->query($remsql);

  $conn->query($sql);

  $sql = "ALTER TABLE poll_".$PollID." ORDER BY PersonID ASC";
  $conn->query($sql);

  echo json_encode("successfully replaced ".$name."'s dates!");
  CloseCon($conn);
}

if( !isset($_POST['functionname']) ) { /*echo ("no function name");*/ }
else{
    switch($_POST['functionname']) {
      case 'showPoll':
        showPoll($_POST['PollID']);
      break;
      case 'submitDates':
        submitDates($_POST['PollID'], $_POST['name'],  $_POST['data']);
      break;
      case 'replaceDates':
        replaceDates($_POST['PollID'], $_POST['name'], $_POST['ParticipantID'], $_POST['data']);
      break;
    }
}

?>
