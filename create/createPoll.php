<?php
include "../serverConnection.php";
require "../libraries/HTMLPurifier/HTMLPurifier.auto.php";

function isValidDate($date){
  if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
      return true;
  } else {
      return false;
  }
}

function nameExists($pollName){
  $conn = OpenCon();

  $sql = "SELECT PollID FROM polls";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      if($pollName == $row["PollID"]){
        echo "true";
        CloseCon($conn);
        return;
      }
    }
  }
  CloseCon($conn);
  echo "false";
}


function createNewPoll($pollData){

  $conn = OpenCon();

  $sql = "SELECT PollID FROM polls";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      if($pollData["PollID"] == $row["PollID"]){
        //echo $pollData["PollID"]." already exists";
        CloseCon($conn);
        return;
      }
    }
  }

  $PollID = preg_replace("/[^a-zA-Z0-9]+/", "", strval($pollData['PollID']));
  if(strlen($PollID) == 0) { CloseCon($conn); return;}

  $StartDate = strval($pollData['StartDate']);
  $EndDate = strval($pollData['EndDate']);

  if(!isValidDate($StartDate)) { CloseCon($conn);return;}
  if(!isValidDate($EndDate)) { CloseCon($conn);return;}

  if(strtotime($StartDate) > strtotime($EndDate)) {
    //echo "end date before start date";
    CloseCon($conn);
    return;
  }

  $config = HTMLPurifier_Config::createDefault();
  $config->set('HTML.Allowed', 'p,ul,ol,li,br,i,b,strong,em,small,del,ins,sub,sup');
  $purifier = new HTMLPurifier($config);
  $PollDescription =  $purifier->purify( strval($pollData['PollDescription']) );
  $PollDescription = str_replace("'", "''", $PollDescription);

  $config = HTMLPurifier_Config::createDefault();
  $config->set('HTML.Allowed', 'i,b,strong,em,small,del,ins,sub,sup');
  $purifier = new HTMLPurifier($config);
  $PollTitle = $purifier->purify( strval($pollData['PollTitle']) );
  $PollTitle = str_replace("'", "''", $PollTitle);

  $PollLocation = $purifier->purify( strval($pollData['PollLocation']) );
  $PollLocation = str_replace("'", "''", $PollLocation);


  //echo "creating new Poll";

  //create poll information entry in polls table
  $sql = "
    INSERT INTO polls VALUES(
      '".$PollID."',
      '".$PollTitle."',
      '".$PollLocation."',
      '".$PollDescription."',
      '".$StartDate."',
      '".$EndDate."'
    );
  ";

  $conn->query($sql);

  //create poll participant table
  $sql = "
      CREATE TABLE poll_".$PollID." (
      PersonID int NOT NULL AUTO_INCREMENT,
      PersonName text,";

  //add date columns
  $period = new DatePeriod(
       new DateTime($StartDate),
       new DateInterval('P1D'),
       new DateTime($EndDate." 23:59:59")
  );

  $allDates = iterator_to_array($period);

  foreach($allDates as $date){
    $sql = $sql."date_".$date->format('Ymd')." int,";
  }

  $sql = $sql."PRIMARY KEY (PersonID) );";

  $conn->query($sql);

  echo $PollID;

  /*$sql = "SELECT PollDescription FROM polls";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<div>".$row["PollDescription"]."</div>";
    }
  }*/

  CloseCon($conn);
}


if( !isset($_POST['functionname']) ) { /*echo ("no function name");*/ }
else{
    switch($_POST['functionname']) {
        case 'createNewPoll':
               createNewPoll($_POST['pollData']);
           break;
         case 'nameExists':
                nameExists($_POST['pollName']);
            break;
    }
}

?>
