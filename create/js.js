var doesNameExist = false;
var startDate = new Date().toISOString().slice(0, 10)
var endDate = new Date().toISOString().slice(0, 10)
var datePicker;

function checkAutoInserts(ed){
  content = ed.contentDocument.getElementById("tinymce").lastChild.textContent;
  element = ed.contentDocument.getElementById("tinymce").lastChild;

  if(content == "-"){
    ed.undoManager.ignore(function(){
      ed.execCommand('mceInsertContent', false, ' ');
    });
    ed.undoManager.beforeChange()
    ed.undoManager.add()

    ed.undoManager.ignore(function(){
      ed.execCommand('InsertUnorderedList', false, {
        'list-style-type': 'disc',
      });
      ed.execCommand('mceSelectNode', false, ed.contentDocument.getElementById("tinymce").lastChild.lastChild);
      ed.execCommand('mceReplaceContent', false, "");
    });

    ed.undoManager.add()

    event.preventDefault();
    return false;
  }else if(content == "1."){
    ed.undoManager.ignore(function(){
      ed.execCommand('mceInsertContent', false, ' ');
    });
    ed.undoManager.beforeChange()
    ed.undoManager.add()

    ed.undoManager.ignore(function(){
      ed.execCommand('InsertOrderedList', false, {
        'list-style-type': 'decimal',
      });
      ed.execCommand('mceSelectNode', false, ed.contentDocument.getElementById("tinymce").lastChild.lastChild);
      ed.execCommand('mceReplaceContent', false, "");
    });

    ed.undoManager.add()

    event.preventDefault();
    return false;
  }

  return true;
}

tinymce.init({
  selector: '#poll-description',
  resize: true,
  menubar: false,
  content_css: "tinyMCE.css",
  plugins: 'lists',
  toolbar: 'undo redo | bold italic | numlist bullist',
  setup: function(ed) {
    ed.on('keydown', function(event) {
        if (event.keyCode == 9) { // tab pressed
          if (event.shiftKey) {
            ed.execCommand('Outdent');
          }
          else {
            ed.execCommand('Indent');
          }
          event.preventDefault();
          return false;
        }

        if(event.keyCode == 32){// space pressed
          return checkAutoInserts(ed);
        }

    });

    ed.on('init', function(event){
      datePicker.show();
      datePicker.elementChanged();
    });
  }
});



$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'right',
    autoApply: true,
  }, function(start, end, label) {
    startDate = start.format('YYYY-MM-DD');
    endDate = end.format('YYYY-MM-DD');
  });

  datePicker = $('#daterange').data('daterangepicker');
  datePicker.show();
  datePicker.elementChanged();
});



//$('input[name="daterange"]').daterangepicker.renderCalendar();

function createNewPoll(){

  if(doesNameExist){
    console.log("name already exists");
    $("#error-field").html("name already exists");
    return;
  }

  if($('#poll-id').val() == ""){
    console.log("Name can't be left empty");
    $("#error-field").html("Name can't be left empty");
    return;
  }

  if(!validator.isAlphanumeric($('#poll-id').val())){
    console.log("only alphanumeric values allowed for the Name");
    $("#error-field").html("only alphanumeric values allowed for the Name");
    return;
  }

  if($('#poll-id').val().length > 255){
    console.log("name is too long");
    $("#error-field").html("name is too long");
    return;
  }

  /*if(Date.parse(new Date( $('#end-date').val() ) ) < Date.parse(new Date( $('#start-date').val() ) ) ){
    console.log("end date needs to be after start date");
    $("#error-field").html("end date needs to be after start date");
    return;
  }*/

  console.log("created new poll");

  jQuery.ajax({
    type: "POST",
    url: 'createPoll.php',
    dataType: 'text',
    data: {
      functionname: 'createNewPoll',
      pollData: {
        PollID: $('#poll-id').val(),
        PollTitle: $('#poll-title').val(),
        PollLocation: $('#poll-location').val(),
        PollDescription: tinyMCE.activeEditor.getContent(),
        StartDate: startDate,
        EndDate: endDate
      }
    },

    success: function (obj, textstatus) {
              //$(document.body).html(obj);
              //console.log(obj);
              window.location.href = "http://poll.shelley.de/" + obj;
            }
  });

}

function checkIfNameIsAvailable(){
  jQuery.ajax({
    type: "POST",
    url: 'createPoll.php',
    dataType: 'text',
    data: {
      functionname: 'nameExists',
      pollName: $('#poll-id').val()
    },

    success: function (obj, textstatus) {
              if(obj == "true") doesNameExist = true;
              if(obj == "false") doesNameExist = false;
            }
  });
}
