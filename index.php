<?php
  $path = ltrim($_SERVER['REQUEST_URI'], '/');
  $elements = explode('/', $path);
  if(empty($elements[0])) {
      header("Location: http://poll.shelley.de/create");
      exit();
  }
?>

<header>

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="css.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <!--<script src="https://unpkg.com/validator@latest/validator.min.js"></script>-->
  <script type="text/javascript">
    pollID = "<?php
      $path = ltrim($_SERVER['REQUEST_URI'], '/');
      $elements = explode('/', $path);
      echo array_shift($elements);
    ?>";
  </script>
  <script src="js.js"></script>
</header>
<body class="no-scrollbar">

  <div id="source-svgs" style="display:none"></div>
  <div id="copy-elements" style="display:none">
        <div class="green-checkmark" id="copy-green-checkmark">
          <div class="icon-replace-checkmark"></div>
        </div>

        <div class="orange-checkmark-tentative" id="copy-orange-checkmark-tentative">
            <div class="icon-replace-checkmark-tentative"></div>
        </div>

        <div class="triple-state-checkbox not-going" id="copy-triple-state-checkbox">
          <div class="checkfield">
            <div class="icon-replace-checkmark"></div>
            <div class="icon-replace-checkmark-tentative"></div>
          </div>
        </div>

  </div>

  <div id="poll-title"></div>
  <div id="poll-location">
    <div class="icon-replace-location"></div>
  </div>
  <div id="poll-description">
    <div class="icon-replace-description"></div>
  </div>
  <div id="tentative-explanation">
    <div class="icon-replace-checkmark-tentative"></div>
    <i>Tick the checkboxes twice to select "Yes, if need be" vote</i>
  </div>
  <div id="poll" class="no-scrollbar styled-scrollbars"></div>
</body>
